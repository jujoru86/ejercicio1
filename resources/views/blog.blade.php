@extends('layouts.app')

@section('content')

    <h3>{{$titulo}}</h3>
    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="https://via.placeholder.com/250x100" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="{{url('entry/1')}}" class="btn btn-primary">Go somewhere</a>
        </div>
    </div>
@endsection
