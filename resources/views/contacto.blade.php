@extends('layouts.app')

@section('content')

    <div class="card">
        <form class="form-horizontal px-1" method="POST" action="{{ route('contact_form')}}">

            <fieldset>

                <!-- Form Name -->
                <legend>Contacto</legend>
                <hr>
                <!-- Text input-->
                @csrf <!-- Toke de seguridad -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nombre">Nombre</label>
                    <div class="col-md-4">
                        <input id="nombre" name="nombre" type="text" placeholder="" class="form-control input-md">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">Email</label>
                    <div class="col-md-4">
                        <input id="email" name="email" type="text" placeholder="" class="form-control input-md">

                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textarea">Mensaje</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="mensaje" name="mensaje"></textarea>
                    </div>
                </div>
                <div class="form-group">
                <input type="submit" class="btn btn-success" name="enviar" value="Enviar"/>
                </div>
            </fieldset>
        </form>

    </div>
@endsection
