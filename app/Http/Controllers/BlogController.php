<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {

        return view('blog', ['titulo' => "Mi blog AT"]);
    }

    public function show()
    {

    }

    public function getEntry($id)
    {
        return view('entry', ['titulo' => "Mi blog AT"]);
    }
}
